from setuptools import setup, find_packages

version = "0.0.0"

# Required packages for your project can be listed here
# Updating this list with other packages you import in your project
# will make sure that they are installed when your project is installed.
# That makes it much easier to use the project on other computers.

requirements = [
    'matplotlib'
]

setup(
    name="easy_plot",
    version=version,
    description="Rocket reinforcement learning project",
    install_requires = requirements,
    python_requires=">=3.5",
    packages=find_packages(),
)
